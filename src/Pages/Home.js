import {Text, View, TouchableOpacity, StyleSheet,Button, } from 'react-native';
import React, {Component} from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
const Tombol = ({label, onPress, }) => {
    return(
        <TouchableOpacity onPress={onPress} style={style.container}>
            <Text>{label}</Text>
        </TouchableOpacity>
    );
};

export class Home extends Component {
    render() {
        return (
            <View>
                <Button 
                label={'PartSatu'}
                    onPress={()=> this.props.navigation.navigate('PartSatu')}
                    title="Part Satu"
                />
                <Button
                label={'Part Dua'}
                     onPress={()=> this.props.navigation.navigate('PartSatu')}
                    title="Part Dua"
                />
                <Button
                label={'Part Tiga'}
                     onPress={()=> this.props.navigation.navigate('PartSatu')}
                    title="Part Tiga"
                />
                <Text>hiuuu</Text>
            </View>
        );
    }
}
 const Tab = createBottomTabNavigator();
export function App() {
  return (
    <NavigationContainer>
      <Tab.Navigator>
        <Tab.Screen name="Home" component={Home} />
        <Tab.Screen name="Settings" component={PartSatu} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}


export default Home;

const style = StyleSheet.create({
    container: {
        margin : 10,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#3385ff',
    },
});