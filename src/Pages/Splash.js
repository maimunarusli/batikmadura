import React, {useEffect} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import Foto from '../assets/like.png';


const Splash =({navigation}) => {
    useEffect(() => {
     setTimeout (() => {
         navigation.replace('Home');
     },1000);
});
return (
    <View style={styles.wrapper}>
        <Image source={Foto} 
        style={{height:100,width:100}}>
        </Image>
        <Text style={{fontSize:20,fontWeight:'bold',color:'green'}}>Hello code World </Text>
    </View>     
    );
};

export default Splash;

const styles = StyleSheet.create({
    wrapper: {
        backgroundColor:'white',
        alignItems: 'center',
        justifyContent: 'center',
        flex :1,

    },
    logo:{
        margin: 10,
    },
});
