import {View, Text} from 'react-native'
import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {PartSatu, Home, Splash,} from '../Pages/Index';
const Navigasi = createNativeStackNavigator();

const Route = () => {
  return (
    <Navigasi.Navigator
    initialRouteName='Splash'
    screenOptions={{headerShown: false}}>
      <Navigasi.Screen name="Home" component={Home} />
      <Navigasi.Screen name="PartSatu" component={PartSatu} />
      <Navigasi.Screen name="Splash" component={Splash} />
    </Navigasi.Navigator>
  );
};

export default Route;
